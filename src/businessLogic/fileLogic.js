import { setAlert } from '../actions/actions_helper.js';
import settings from '../../settings';

export function downloadFile(downloadPath, filePath, appState, dispatch) {
  const { authToken, tokenType, client, expiry, uid } = appState.session;
  const downloader = require('file-downloader');

	window.downloader.get(downloadPath, filePath, {
     'Accept': settings.apiVersionHeader,
     'Access-Token': authToken,
     'Client': client,
     'Token-Type': tokenType,
     'Expiry': expiry,
     'uid': uid
   })
   .catch(error => dispatch(setAlert(`Cannot download ${filePath}`)));
   //  .then(fileName => console.log(`File ${fileName} has been downloaded!`));

}

// chain

export function downloadChainOutputFile(filePath, processChainId, appState, dispatch) {
  // /api/process_steps/:id/download_output_file
  const downloadPath = `${settings.apiBaseUrl}/api/process_chains/${processChainId}/download_output_file?relative_path=${filePath}`;
  downloadFile(downloadPath, filePath, appState, dispatch);
}

export function downloadChainOutputZip(processChainId, appState, dispatch) {
  // /api/process_steps/:id/download_output_zip
  const downloadPath = `${settings.apiBaseUrl}/api/process_chains/${processChainId}/download_output_zip`;
  downloadFile(downloadPath, "output.zip", appState, dispatch);
}

export function downloadInputZip(processChainId, appState, dispatch) {
  // /api/process_chains/:id/download_input_zip
  const downloadPath = `${settings.apiBaseUrl}/api/process_chains/${processChainId}/download_input_zip`;
  downloadFile(downloadPath, "input.zip", appState, dispatch);
}

export function downloadInputFile(filePath, processChainId, appState, dispatch) {
  // /api/process_chains/:id/download_input_file
  const downloadPath = `${settings.apiBaseUrl}/api/process_chains/${processChainId}/download_input_file?relative_path=${filePath}`;
  downloadFile(downloadPath, filePath, appState, dispatch);
}

// step

export function downloadStepOutputZip(processStepId, appState, dispatch) {
  // /api/process_steps/:id/download_output_zip
  const downloadPath = `${settings.apiBaseUrl}/api/process_steps/${processStepId}/download_output_zip`;
  downloadFile(downloadPath, "output.zip", appState, dispatch);
}

export function downloadOutputFile(filePath, processStepId, appState, dispatch) {
  // /api/process_steps/:id/download_output_file
  const downloadPath = `${settings.apiBaseUrl}/api/process_steps/${processStepId}/download_output_file?relative_path=${filePath}`;
  downloadFile(downloadPath, filePath, appState, dispatch);
}
