import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';
import * as actions from '../actions/recipeActions';

import _ from 'lodash';

export class RecipeListItem extends Component {

  itemClass(isPublic) {
    if(isPublic == true) {
      return("recipe-item");
    }
    return("recipe-item recipe-item__private");
  }

  renderDescription(description) {
    if(_.isUndefined(description) || _.isNull(description) || description=="") {
      return (
        <span className="small-info-text">none</span>
      );
    }
    return description;
  }

  render() {
    let { recipe } = this.props;

    return(
      <Link to={`/recipes/${recipe.id}`}>
        <li className={this.itemClass(recipe.public)}>
          <div className="recipe-item__header recipe-item__unselected">
            {recipe.name}
          </div>
          <div className="recipe-item__body">
            {this.renderDescription(recipe.description)}
          </div>
        </li>
      </Link>
    );
  }
}

RecipeListItem.propTypes = {
  recipe: PropTypes.object.isRequired,
  isSelected: PropTypes.bool.isRequired
};

export default RecipeListItem;
