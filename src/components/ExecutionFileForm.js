import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../actions/recipeActions';

export class ExecutionFileForm extends Component {

  handleExecution = (e) => {
    e.preventDefault();
    const { dispatch, appState } = this.props;
    const input_files = ReactDOM.findDOMNode(this.refs.input_files).files;
    const form_data = new FormData();

    for (let file of input_files) {
      form_data.append('input_files[]', file);
    }

    const { signedIn, authToken, tokenType, client, expiry, uid } = appState.session;
    dispatch(actions.executeRecipe(this.props.recipe.id, form_data, signedIn, authToken, tokenType, client, expiry, uid));
    ReactDOM.findDOMNode(this.refs.fileUploadForm).reset();
  }

  render() {
    let { recipe } = this.props;

    return(
      <form ref="fileUploadForm">
        <div className="choose-file-container">
          <input className="file-picker" encType="multipart/form-data" multiple type="file" name="input_files[]" label="Input File" ref="input_files" id="input_files" />
          <br/>
          <button className="file-button" onClick={this.handleExecution}>GO!</button>
        </div>
      </form>
    );
  }
}

ExecutionFileForm.propTypes = {
  recipe: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  appState: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    appState: state.appState
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    dispatch: dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExecutionFileForm);
