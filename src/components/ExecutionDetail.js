import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';

import ExecutionDetailStep from './ExecutionDetailStep';
import ExecutionFileList from './ExecutionFileList';
import TimeAgo from 'react-timeago';

export class ExecutionDetail extends Component {

  renderFinishedTitle() {
    let { processChain } = this.props;
    return(<span>Finished processing <TimeAgo date={processChain.finished_at} /> </span>);
  }

  processChainChannel(processChainId) {
    return(`process_chain_${processChainId}`);
  }

  renderInProgressTitle() {
    let { processChain } = this.props;
    if(_.isNil(processChain.executed_at)) {
      return(<span>Waiting to be processed (submitted <TimeAgo date={processChain.created_at} />) {processChain.executed_at}</span>);
    }
    return(<span>Processing (started <TimeAgo date={processChain.executed_at} />)</span>);
  }

  renderLinkText(file_name) {
    if(_.isNil(file_name)) {
      return;
    }
    return(<span>{file_name} <span className="fa fa-download"/></span>);
  }

  executionResult() {
    let { processChain } = this.props;
    if(_.isNil(processChain.finished_at)) {
      return(<p><span className="fa fa-gear fa-spin fa-fw" />{this.renderInProgressTitle()}</p>);
    }
    return(<p><span className="fa fa-check" /> {this.renderFinishedTitle()}</p>);
  }

  render() {
    const { processChain, dispatch, appState } = this.props;
    return (
      <div className="execution-result-border">
        {this.executionResult()}

        <ExecutionFileList key={processChain.id}
          processChain={processChain}
          inputFileManifest={processChain.input_file_manifest}
        />
        <table className="table">
          <thead>
            <tr>
              <th>status</th>
              <th>Step name</th>
              <th>Output</th>
            </tr>
          </thead>
          <tbody>
            {processChain.process_steps.map(step => <ExecutionDetailStep key={step.id} recipeId={processChain.recipe_id} processStep={step} dispatch={dispatch} appState={appState} />)}
          </tbody>
        </table>
      </div>
    );
  }
}

ExecutionDetail.propTypes = {
  dispatch: PropTypes.func,
  processChain: PropTypes.object.isRequired,
  appState: PropTypes.object
};

function mapStateToProps(state) {
  return { appState: state.appState };
}

function mapDispatchToProps(dispatch) {
  return {
    // actions: bindActionCreators(actions, dispatch),
    dispatch: dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExecutionDetail);
