import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import settings from '../../../settings';

import _ from 'lodash';
import { Link } from 'react-router';
import TimeAgo from 'react-timeago';

export class StatusReport extends Component {

  renderReportStatus(status) {
    let renderedStatus;
    let resultClass;

    if(status == "OK") {
      renderedStatus = <span><span className="fa fa-check"/> OK</span>;
      resultClass = "success";
    } else {
      renderedStatus = status;
    }
    return(<td className={`text status-result-row-cell ${resultClass}`}>{renderedStatus}</td>);
  }

  renderReportName(name) {
    if(name == "Sidekiq") {
      return(<td className="text status-result-row-cell"><Link to={`${settings.apiBaseUrl}/sidekiq`} target="_blank">Sidekiq <span className="fa fa-external-link"/></Link></td>);
    } else {
      return(<td className="text status-result-row-cell">{name}</td>);
    }
  }

  renderReportRow(result) {
    return(
      <tr key={result.name} className="status-result-row">
        {this.renderReportName(result.name)}
        {this.renderReportStatus(result.status)}
          <td className="text status-result-row-cell">{result.message}</td>
      </tr>
    );
  }

  render() {
    let { statusReport } = this.props;
    if(_.isEmpty(this.props.session)) {
      return(
        <p className="help-block disabled">Sign in to see this</p>
      );
    }

    if(this.props.getStatusReportInProgress == true) {
      return(
        <div className="recipe-list-container text-center">
          <span>getting status report...</span>
        </div>
      );
    } else if(_.isNil(statusReport)) {
      return(<div>Could not get status report. What is going on up there?</div>);
    } else if(_.isEmpty(statusReport)) {
      return(<div>The status report does not look right. Please check the server.</div>);
    }

    return (
      <div>
        <div>At {statusReport.timestamp} (<TimeAgo date={statusReport.timestamp} />)</div>
        <ul className="status-report-container">
          <table>
            <thead>
              <tr>
                <th>Service</th>
                <th>Status</th>
                <th>Message</th>
              </tr>
            </thead>
            <tbody>
            { statusReport.results.map(result => this.renderReportRow(result)) }
            </tbody>
          </table>
        </ul>
      </div>
    );
  }
}

StatusReport.propTypes = {
  session: PropTypes.object,
  getStatusReportInProgress: PropTypes.bool.isRequired,
  statusReport: PropTypes.object
};

export default StatusReport;
