import React, { PropTypes } from 'react';
import AccountListItem from './AccountListItem.js';
import _ from 'lodash';
import { Link } from 'react-router';

const AccountsList = (props) => {
  if(_.isEmpty(props.session)) {
    return(
      <p className="help-block disabled">Sign in as an admin to see accounts</p>
    );
  }

  if(props.getAccountsInProgress === true) {
    return(
      <div className="recipe-list-container text-center">
        <span>Retrieving accounts in progress...</span>
      </div>
    );
  }

  return (
    <div className="account-list-container">
      { props.accounts.map(account =>
        <AccountListItem key={account.id}
        account={account} />)
      }
    </div>
  );
};

AccountsList.propTypes = {
  session: PropTypes.object,
  getAccountsInProgress: PropTypes.bool.isRequired,
  accounts: PropTypes.array.isRequired
};

export default AccountsList;
