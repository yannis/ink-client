import React, {PropTypes} from 'react';
import AlertWidget from './AlertWidget.js';

export function alertClasses(pageName) {
  if(pageName == "SignInPage") {
    return("alerts-list--sign-in-page");
  }
  return("alerts-list");
}

const AlertList = (props) => {

  return (
    <div className={alertClasses(props.pageName)}>
      {props.alerts.map(alert => <AlertWidget key={alert.id} content={alert.content} type={alert.type} />)}
    </div>
  );
};

AlertList.propTypes = {
  alerts: PropTypes.array.isRequired,
  pageName: PropTypes.string
};

export default AlertList;
