import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _  from "lodash";
import SmoothCollapse from 'react-smooth-collapse';

import * as actions from '../actions/authenticationActions';
import { downloadOutputFile, downloadStepOutputZip } from '../businessLogic/fileLogic';
import { expandStepFileList, collapseStepFileList } from '../actions/recipeActions';

export class ExecutionDetailStep extends Component {

  handleOutputFileDownload = (e, fileName, processStepId) => {
    e.preventDefault();
    const { appState, dispatch } = this.props;
    dispatch(downloadOutputFile(fileName, processStepId, appState, dispatch));
  }

  handleStepOutputZipDownload = (e, processStepId) => {
    e.preventDefault();
    const { appState, dispatch } = this.props;
    dispatch(downloadStepOutputZip(processStepId, appState, dispatch));
  }

  handleToggle = (e, toExpand, processStepId, chainId, recipeId) => {
    e.preventDefault();
    const { appState, dispatch } = this.props;
    if (toExpand == true) {
      dispatch(expandStepFileList(processStepId, chainId, recipeId));
    } else {
      dispatch(collapseStepFileList(processStepId, chainId, recipeId));
    }
  }

  listExpandCollapseToggle(processStep) {
    return(
      <a href="#" onClick={e => this.handleToggle(e, processStep.file_list_collapse, processStep.id, processStep.process_chain_id, this.props.recipeId)}>
        <span className={this.listIcon(processStep.file_list_collapse)}/>
        &nbsp;Files ({_.size(processStep.output_file_manifest)})
      </a>
    );
  }

  listIcon(collapsed) {
    if(collapsed == true || _.isNil(collapsed)) {
      return "fa fa-caret-right";
    } else {
      return "fa fa-caret-down";
    }
  }

  successClass(execution_errors, successful) {
    if(successful === true) {
      return("success");
    }
    else if(!_.isNil(execution_errors) && !_.isEmpty(execution_errors)) {
      return("fail");
    }
    return("");
  }

  renderIcon(processStep) {
    if(processStep.in_progress === true) {
      return(<span className="fa fa-gear fa-spin fa-2x fa-fw step-processing-centered" />);
    }
    else if(_.isNil(processStep.started_at)) {
      return(<span className="small-info-text fa fa-question"/>);
    }
    else if(processStep.successful === true) {
      return(<span className="fa fa-check"/>);
    }
    else if(!_.isNil(processStep.execution_errors) && !_.isEmpty(processStep.execution_errors)) {
      return(<span className="fa fa-times"/>);
    }
  }

  renderLink(filePath, processStepId, finished_at) {
    if(!_.isNil(filePath)) {
      return(
        <a href="#" onClick={e => this.handleOutputFileDownload(e, filePath, processStepId)}>{filePath} <span className="fa fa-download"/></a>
      );
    }
    return(
      <span className="small-info-text">---</span>
    );
  }

  renderZipLink(processStep) {
    if(_.isNil(processStep.finished_at)) {
      return(<span className="small-info-text">no files available yet <span className="fa fa-download"/></span>);
    }

    return(
      <a href="#" onClick={e => this.handleStepOutputZipDownload(e, processStep.id)}>all files (zip) <span className="fa fa-download"/></a>
    );
  }

  renderFileList(fileList) {
    let { processStep } = this.props;
    let collapsed = processStep.file_list_collapse;
    if(_.isNil(fileList)) {
      return (<span className="small-info"><span className="fa fa-ban"/></span>);
    }
    return(
      <SmoothCollapse expanded={!collapsed}>
        {fileList.map( file =>
          <li key={file.path} className="file-list-item monospace">
            <span className="fa fa-hand-o-right small-info"/>
            {this.renderLink(file.path, processStep.id, processStep.finished_at)}
            <span className="small-info-text">({file.size})</span>
          </li>)}
      </SmoothCollapse>
    );
  }

  renderOutputFiles(processStep) {
    let fileList = processStep.output_file_manifest;
    return(
      <div>
        <ul className="file-list">
          {this.listExpandCollapseToggle(processStep)}
          {this.renderFileList(fileList)}
        </ul>
      </div>
    );
  }

  renderProcessLogLink(processStep) {
    if(_.isNil(processStep.finished_at)) {
      return null;
    }
    let processLogLocation = processStep.process_log_location;
    return(
      <div>
        <span className="fa fa-hdd-o"/> Log: { this.renderLink(processLogLocation, processStep.id, processStep.finished_at) }
      </div>
    );
  }

  renderErrors(processStep) {
    let errors = processStep.execution_errors;

    if(_.isNil(processStep.finished_at)) {
      errors = <span/>;
    }
    else if(_.isNil(errors) || _.isEmpty(errors)) {
      errors = <span className="small-info-text">none</span>;
    }

    return(
      <div>Errors: <span className="fail">{ errors }</span></div>
    );
  }

  renderNotes(processStep) {
    let notes = processStep.notes;

    if(_.isNil(processStep.finished_at)) {
      notes = <span/>;
    }
    else if(_.isNil(notes) || _.isEmpty(notes)) {
      notes = <span className="small-info-text">none</span>;
    }

    return(
      <div>Notes: { notes }</div>
    );
  }

  renderOutput(processStep) {
    if(_.isNil(processStep.finished_at) && _.isNil(processStep.output_file_manifest)) {
      return(<span className="fa fa-hourglass-o small-info"/>);
    }
    return(
      <div>
        { this.renderZipLink(processStep) }
        { this.renderOutputFiles(processStep) }
        { this.renderErrors(processStep) }
        { this.renderNotes(processStep) }
        { this.renderProcessLogLink(processStep) }
      </div>
    );
  }

  outputRowClasses(processStep) {
    if(_.isNil(processStep.finished_at) && _.isNil(processStep.output_file_manifest)) {
      return("cell centered");
    }
    return("cell");
  }

  renderVersion(version) {
    if(_.isEmpty(version)) {
      return;
    }
    return(
      <div><span className="small-info-text">version {version}</span></div>
    );
  }

  stepShorthand(stepName) {
    let array = stepName.split("::");
    return array[array.length - 1];
  }

  renderStepShorthand(stepName) {
    let { processStep } = this.props;
    let className = "";
    if(processStep.in_progress === true) {
      className = "bold";
    }
    return(<div><span className={className}>{this.stepShorthand(stepName)}</span></div>);
  }

  render() {
    const { processStep } = this.props;

    return(
      <tr>
        <td className={`cell centered ${this.successClass(processStep.execution_errors, processStep.successful)}`}>{this.renderIcon(processStep)}</td>
        <td className="cell centered">
          {this.renderStepShorthand(processStep.step_class_name)}
          {this.renderVersion(processStep.version)}
        </td>
        <td className={this.outputRowClasses(processStep)}>
          { this.renderOutput(processStep) }
        </td>
      </tr>
    );
  }
}

ExecutionDetailStep.propTypes = {
  dispatch: PropTypes.func,
  processStep: PropTypes.object.isRequired,
  appState: PropTypes.object,
  recipeId: PropTypes.number.isRequired
};

function mapStateToProps(state) {
  return { appState: state.appState };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    dispatch: dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExecutionDetailStep);
