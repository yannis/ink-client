import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ReactDOM from 'react-dom';
import _ from 'lodash';
import Select from 'react-select';
import 'react-select/dist/react-select.css';

import * as actions from '../actions/recipeActions';

import StepClassEditableList from './StepClassEditableList';

export class StepClassForm extends Component {

  componentDidMount = () => {
    const { appState, dispatch } = this.props;
    dispatch(actions.getAvailableStepClasses(appState));
  }

  handleAddStepClass = (e) => {
    e.preventDefault();
    const { appState, dispatch } = this.props;
    const { stepClassList } = appState;
    const newStepClass = this.refs.step_class_name.value;
    dispatch(actions.addToStepList(newStepClass));
    ReactDOM.findDOMNode(this.refs.step_class_name).value = "";
  }

  addStepClass = (option) => {
    let value = option.value;
    const { appState, dispatch } = this.props;
    dispatch(actions.addToStepList(value));
    return;
  }

  renderAvailableStepClasses = () => {
    const { appState } = this.props;

    if(_.isNil(appState.availableStepClassList)) {
      return(
        <div className="button">
          <input className="input--inline-box" type="text" label="Name" ref="step_class_name" id="step" />
          <button onClick={this.handleAddStepClass} className="inline-action-button" value="asdf"><span className="fa fa-plus"/> add step</button>
        </div>
      );
    }

    let options = appState.availableStepClassList.map((klass,i) => ({name, value: klass.name, label: klass.name}));

    return(
      <div className="input--dropdown">
        <Select
          name="step_class_name"
          ref="step_class_name"
          options={options}
          onChange={this.handleChange}
        />
      </div>
    );
  }

  handleChange = (value) => {
    this.addStepClass(value);
  }

  render() {
    const { appState, stepClassList } = this.props;
    return (
      <div>
        <div className="form-container">
          {this.renderAvailableStepClasses()}
        </div>
        <ol>
          { stepClassList.map((stepClassName, index) =>
            <StepClassEditableList key={index}
            index={index}
            stepClassName={stepClassName} />)
          }
        </ol>
      </div>
    );
  }
}

StepClassForm.propTypes = {
  dispatch: PropTypes.func.isRequired,
  stepClassList: PropTypes.array.isRequired,
  appState: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    appState: state.appState
  };
}

export default connect(
  mapStateToProps
)(StepClassForm);
