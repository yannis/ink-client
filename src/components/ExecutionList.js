import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../actions/authenticationActions';
import _ from 'lodash';

import ExecutionDetail from './ExecutionDetail';
import ExecutionPlaceholder from './ExecutionPlaceholder';

import * as eventActions from '../constants/PusherActions';
import { subscribe, unsubscribe } from 'pusher-redux';

export class ExecutionList extends Component {

  constructor(props, context) {
    super(props, context);
    this.subscribe = this.subscribe.bind(this);
    this.unsubscribe = this.unsubscribe.bind(this);
   }

  componentWillMount() {
    this.subscribe();
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  subscribe() {
    // let chainId = this.props.processChain.id;
    // let channel = this.processChainChannel(chainId);
    let channel = "process_chain_execution";

    subscribe(channel, 'processing_started', eventActions.MARK_CHAIN_AS_STARTED);
    subscribe(channel, 'processing_completed', eventActions.MARK_CHAIN_AS_COMPLETED);
    subscribe(channel, 'processing_error', eventActions.MARK_CHAIN_AS_ERRORED);
    subscribe(channel, 'process_step_started', eventActions.MARK_STEP_AS_STARTED);
    subscribe(channel, 'process_step_completed', eventActions.MARK_STEP_AS_COMPLETED);
  }

  unsubscribe() {
    // let chainId = this.props.processChain.id;
    // let channel = this.processChainChannel(chainId);
    let channel = "process_chain_execution";

    unsubscribe(channel, 'processing_started', eventActions.MARK_CHAIN_AS_STARTED);
    unsubscribe(channel, 'processing_completed', eventActions.MARK_CHAIN_AS_COMPLETED);
    unsubscribe(channel, 'processing_error', eventActions.MARK_CHAIN_AS_ERRORED);
    unsubscribe(channel, 'process_step_started', eventActions.MARK_STEP_AS_STARTED);
    unsubscribe(channel, 'process_step_completed', eventActions.MARK_STEP_AS_COMPLETED);
  }

  renderPlaceholders(count) {
    return(
      <div className="placeholder-container">
        {[...Array(count)].map((x, i) =>
          <ExecutionPlaceholder key={i + 1} />
        )}
      </div>
    );
  }

  render() {
    let { session, recipe, executionPlaceholderCount } = this.props;

    if(_.isNull(session.account)) {
      return(
        <div className="light-border disabled">
          <h4>Finished process chains<span className="fa fa-file"/></h4>
        </div>
      );
    }

    if(recipe) {
      return (
        <div>
          <div className="execution-result-border__top">
            { this.renderPlaceholders(executionPlaceholderCount) }
            {recipe.process_chains.map(chain =>
              <ExecutionDetail
                key={chain.id}
                processChain={chain}
              />)
            }
          </div>
        </div>
      );
    }

    return(
      <div className="light-border disabled">
        <h4>Finished process chains</h4>
        <p className="help-block disabled">No recipe selected</p>
      </div>
    );
  }
}

ExecutionList.propTypes = {
  recipe: PropTypes.object,
  session: PropTypes.object,
  executionPlaceholderCount: PropTypes.number.isRequired
};

function mapStateToProps(state) {
  return { appState: state.appState };
}

function mapDispatchToProps(dispatch) {
  return {
    eventActions: bindActionCreators(eventActions, dispatch),
    dispatch: dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExecutionList);
