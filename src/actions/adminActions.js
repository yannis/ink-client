import * as actions from '../constants/ActionTypes';
import { checkStatus, setAlert } from './actions_helper.js';
import { browserHistory } from 'react-router';
import * as AlertTypes from '../constants/AlertTypes';

import _ from 'lodash';

import settings from '../../settings';

export function getAllAccounts(appState) {

	const { authToken, tokenType, client, expiry, uid } = appState.session;
	const signedIn = (authToken != null);

	return(getAllAccountsWithTokens(signedIn, authToken, tokenType, client, expiry, uid));
}

export function getAllAccountsWithTokens(signedIn, authToken, tokenType, client, expiry, uid) {

  return function(dispatch) {
		let theResponse;
    dispatch({type: actions.GET_ALL_ACCOUNTS_REQUEST});
    fetch(`${settings.apiBaseUrl}/api/admin/accounts`, {
      method: 'GET',
			headers: {
        'Accept': settings.apiVersionHeader,
        'Content-Type': 'application/json',
        'Access-Token': authToken,
        'Client': client,
        'Token-Type': tokenType,
        'Expiry': expiry,
        'uid': uid
      }
    })
		.then(response => {
			theResponse = response;
			return response.json();
		})
		.then(json => {
			checkStatus(theResponse, dispatch, signedIn);
			if(_.isUndefined(json.accounts)) {
				let error = new Error(theResponse.statusText);
				error.response = json.errors;
				throw error;
			}
			else {
				dispatch(successGetAllAccounts(json));
			}
		})
		.catch(error => {
			dispatch(setAlert(error.response, AlertTypes.ERROR));
			dispatch(rejectGetAllAccounts(error));
    });
		return null;
  };
}

export function successGetAllAccounts(json) {
  return {
    type: actions.GET_ALL_ACCOUNTS_SUCCESS,
    accounts: json.accounts
  };
}

export function rejectGetAllAccounts(error) {
  return {
    type: actions.GET_ALL_ACCOUNTS_FAILURE,
    error: error.toString()
  };
}

export function getStatusReport(appState) {

	const { authToken, tokenType, client, expiry, uid } = appState.session;
	const signedIn = (authToken != null);

	return(getStatusReportWithTokens(signedIn, authToken, tokenType, client, expiry, uid));
}

export function getStatusReportWithTokens(signedIn, authToken, tokenType, client, expiry, uid) {

  return function(dispatch) {
		let theResponse;
    dispatch({type: actions.GET_STATUS_REPORT_REQUEST});
    fetch(`${settings.apiBaseUrl}/check.json`, {
      method: 'GET',
			headers: {
        'Accept': settings.apiVersionHeader,
        'Content-Type': 'application/json',
        'Access-Token': authToken,
        'Client': client,
        'Token-Type': tokenType,
        'Expiry': expiry,
        'uid': uid
      }
    })
		.then(response => {
			theResponse = response;
			return response.json();
		})
		.then(json => {
			checkStatus(theResponse, dispatch, signedIn);
			if(_.isUndefined(json.results)) {
				let error = new Error(theResponse.statusText);
				error.response = json.errors;
				throw error;
			}
			else {
				dispatch(successGetStatusReport(json));
			}
		})
		.catch(error => {
			dispatch(setAlert(error.response, AlertTypes.ERROR));
			dispatch(rejectGetStatusReport(error));
    });
		return null;
  };
}

export function successGetStatusReport(json) {
  return {
    type: actions.GET_STATUS_REPORT_SUCCESS,
    statusReport: json
  };
}

export function rejectGetStatusReport(error) {
  return {
    type: actions.GET_STATUS_REPORT_FAILURE,
    error: error.toString()
  };
}
