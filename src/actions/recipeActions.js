import * as actions from '../constants/ActionTypes';
import { checkStatus, setAlert } from './actions_helper.js';
import { browserHistory } from 'react-router';
import * as AlertTypes from '../constants/AlertTypes';

import _ from 'lodash';

import settings from '../../settings';
///////////////////////////////////////////////////////////////
//////////////// execute single recipe ////////////////////////
///////////////////////////////////////////////////////////////

export function executeRecipe(recipeId, formData, signedIn, authToken, tokenType, client, expiry, uid) {
	if(signedIn === false || signedIn === null || signedIn === undefined) {
		return { type: actions.EXECUTE_RECIPE_REQUEST };
	}

	return function(dispatch) {
		let theResponse;
		dispatch({type: actions.EXECUTE_RECIPE_REQUEST});

		fetch(`${settings.apiBaseUrl}/api/recipes/${recipeId}/execute`, {
			method: 'POST',
			headers: {
				'Accept': settings.apiVersionHeader,
				'Access-Token': authToken,
				'Client': client,
				'Token-Type': tokenType,
				'Expiry': expiry,
				'uid': uid
			},
			body: formData
		})
		.then(response => {
			theResponse = response;
			return response.json();
		})
		.then(json => {
			checkStatus(theResponse, dispatch, signedIn);
			if(_.isUndefined(json.process_chain)) {
        let error = new Error(theResponse.statusText);
        error.response = json.errors;
        throw error;
      }
      else {
        dispatch(setAlert(["Processing started successfully"], AlertTypes.SUCCESS));
				dispatch(successExecuteRecipe(json));
				dispatch(getRecipe(recipeId, signedIn, authToken, tokenType, client, expiry, uid));
      }
		})
		.catch(error => {
			// console.log("error", error)
			dispatch(setAlert(error.response, AlertTypes.ERROR));
			dispatch(rejectExecuteRecipe(error));
		});
		return null;
	};
}

export function successExecuteRecipe(json) {
  return {
    type: actions.EXECUTE_RECIPE_SUCCESS,
    newChain: json.process_chain
  };
}

export function rejectExecuteRecipe(error) {
  return {
    type: actions.EXECUTE_RECIPE_FAILURE,
    error: error.toString()
  };
}

///////////////////////////////////////////////////////////////
////////////////// get single recipe //////////////////////////
///////////////////////////////////////////////////////////////

export function getRecipe(recipeId, appState) {

	const { authToken, tokenType, client, expiry, uid } = appState;
	const signedIn = (authToken != null);

	if(signedIn === false || signedIn === null || signedIn === undefined) {
		return { type: actions.GET_RECIPE_REQUEST };
	}

  return function(dispatch) {
		let theResponse;
		dispatch({type: actions.GET_RECIPE_REQUEST});

    fetch(`${settings.apiBaseUrl}/api/recipes/${recipeId}/`, {
      method: 'GET',
			headers: {
        'Accept': settings.apiVersionHeader,
        'Content-Type': 'application/json',
        'Access-Token': authToken,
        'Client': client,
        'Token-Type': tokenType,
        'Expiry': expiry,
        'uid': uid
      }
    })
		.then(response => {
			theResponse = response;
			return response.json();
		})
		.then(json =>	{
			let signedIn = appState.session ? appState.session.signedIn : false;
			checkStatus(theResponse, dispatch, signedIn);
			if(_.isUndefined(json.recipe)) {
				let error = new Error(theResponse.statusText);
				error.response = json.errors;
				throw error;
			}
			else {
				dispatch(successGetRecipe(json));
			}
		})
		.catch(error => {
			dispatch(setAlert(error.response, AlertTypes.ERROR));
			dispatch(rejectGetRecipe(error));
    });
		return null;
  };
}

export function successGetRecipe(json) {
  return {
    type: actions.GET_RECIPE_SUCCESS,
    recipe: json
  };
}

export function rejectGetRecipe(error) {
  return {
    type: actions.GET_RECIPE_FAILURE,
    error: error.toString()
  };
}

///////////////////////////////////////////////////////////////
/////////////////// get all recipes ///////////////////////////
///////////////////////////////////////////////////////////////


export function getAllRecipes(appState) {

	const { authToken, tokenType, client, expiry, uid } = appState.session;
	const signedIn = (authToken != null);

	return(getAllRecipesWithTokens(signedIn, authToken, tokenType, client, expiry, uid));
}

export function getAllRecipesWithTokens(signedIn, authToken, tokenType, client, expiry, uid) {

  return function(dispatch) {
		let theResponse;
    dispatch({type: actions.GET_ALL_RECIPES_REQUEST});
    fetch(`${settings.apiBaseUrl}/api/recipes`, {
      method: 'GET',
			headers: {
        'Accept': settings.apiVersionHeader,
        'Content-Type': 'application/json',
        'Access-Token': authToken,
        'Client': client,
        'Token-Type': tokenType,
        'Expiry': expiry,
        'uid': uid
      }
    })
		.then(response => {
			theResponse = response;
			return response.json();
		})
		.then(json => {
			checkStatus(theResponse, dispatch, signedIn);
			if(_.isUndefined(json.recipes)) {
				let error = new Error(theResponse.statusText);
				error.response = json.errors;
				throw error;
			}
			else {
				dispatch(successGetAllRecipes(json));
			}
		})
		.catch(error => {
			dispatch(setAlert(error.response, AlertTypes.ERROR));
			dispatch(rejectGetAllRecipes(error));
    });
		return null;
  };
}

export function successGetAllRecipes(json) {
  return {
    type: actions.GET_ALL_RECIPES_SUCCESS,
    data: json
  };
}

export function rejectGetAllRecipes(error) {
  return {
    type: actions.GET_ALL_RECIPES_FAILURE,
    error: error.toString()
  };
}

///////////////////////////////////////////////////////////////
//////////////////// select recipe ////////////////////////////
///////////////////////////////////////////////////////////////

export function selectRecipe(recipeId) {
	return {
		type: actions.SELECT_RECIPE,
		selectedRecipeID: recipeId
	};
}

export function deselectRecipe() {
	return {
		type: actions.DESELECT_RECIPE
	};
}

///////////////////////////////////////////////////////////////
///////// execute recipe - reset process placeholder //////////
///////////////////////////////////////////////////////////////

export function resetPlaceholders() {
	return {
		type: actions.RESET_PLACEHOLDERS
	};
}

///////////////////////////////////////////////////////////////
//////////////// expand/collapse file lists ///////////////////
///////////////////////////////////////////////////////////////

export function expandChainInputFileList(chainId, recipeId) {
	return {
		type: actions.EXPAND_CHAIN_INPUT_FILE_LIST,
		chainId: chainId,
		recipeId: recipeId
	};
}

export function collapseChainInputFileList(chainId, recipeId) {
	return {
		type: actions.COLLAPSE_CHAIN_INPUT_FILE_LIST,
		chainId: chainId,
		recipeId: recipeId
	};
}

export function expandChainOutputFileList(chainId, recipeId) {
	return {
		type: actions.EXPAND_CHAIN_OUTPUT_FILE_LIST,
		chainId: chainId,
		recipeId: recipeId
	};
}

export function collapseChainOutputFileList(chainId, recipeId) {
	return {
		type: actions.COLLAPSE_CHAIN_OUTPUT_FILE_LIST,
		chainId: chainId,
		recipeId: recipeId
	};
}

export function expandStepFileList(stepId, chainId, recipeId) {
	return {
		type: actions.EXPAND_STEP_FILE_LIST,
		chainId: chainId,
		recipeId: recipeId,
		stepId: stepId
	};
}

export function collapseStepFileList(stepId, chainId, recipeId) {
	return {
		type: actions.COLLAPSE_STEP_FILE_LIST,
		chainId: chainId,
		recipeId: recipeId,
		stepId: stepId
	};
}

///////////////////////////////////////////////////////////////
///////////////// new recipe - step class list ////////////////
///////////////////////////////////////////////////////////////

export function resetStepList() {
	return {
		type: actions.RESET_STEP_LIST
	};
}

export function addToStepList(stepClassName) {
	return {
		type: actions.ADD_TO_STEP_LIST,
		stepClassName: stepClassName
	};
}

export function removeFromStepList(index) {
	return {
		type: actions.REMOVE_FROM_STEP_LIST,
		index: index
	};
}

///////////////////////////////////////////////////////////////
////////////////////// create recipe //////////////////////////
///////////////////////////////////////////////////////////////

export function createRecipe(formData, signedIn, authToken, tokenType, client, expiry, uid) {
	if(signedIn === false || signedIn === null || signedIn === undefined) {
		return { type: actions.CREATE_RECIPE_REQUEST };
	}

	return function(dispatch) {
		let theResponse;
		dispatch({type: actions.CREATE_RECIPE_REQUEST});

		fetch(`${settings.apiBaseUrl}/api/recipes`, {
			method: 'POST',
			headers: {
				'Accept': settings.apiVersionHeader,
				'Access-Token': authToken,
				'Client': client,
				'Token-Type': tokenType,
				'Expiry': expiry,
				'uid': uid
			},
			body: formData
		})
		.then(response => {
			theResponse = response;
			return response.json();
		})
		.then(json => {
			checkStatus(theResponse, dispatch, signedIn);
			// get the recipe id from the newly created recipe's JSON response
			if(_.isUndefined(json.recipe)) {
				let error = new Error(theResponse.statusText);
				error.response = json.errors;
				throw error;
			}
			else {
				let recipeId = json.recipe.id;
				dispatch(setAlert("Recipe created!", AlertTypes.SUCCESS));
				dispatch(successCreateRecipe(json.recipe));
				dispatch(resetStepList());
				dispatch(updateRecipeInList(json.recipe));
				browserHistory.push(`/recipes/${recipeId}`);
			}
		})
		.catch(error => {
			dispatch(setAlert(error.response, AlertTypes.ERROR));
			dispatch(rejectCreateRecipe(error));
		});
		return null;
	};
}

export function successCreateRecipe(json) {
	return {
		type: actions.CREATE_RECIPE_SUCCESS,
		recipe: json
	};
}

export function rejectCreateRecipe(error) {
	return {
		type: actions.CREATE_RECIPE_FAILURE,
		error: error.toString()
	};
}

export function updateRecipeInList(recipe) {
	return {
		type: actions.UPDATE_RECIPE_IN_LIST,
		recipe: recipe
	};
}

export function updatedSelectedRecipe(recipe) {
	return {
		type: actions.UPDATE_SELECTED_RECIPE,
		recipe: recipe
	};
}

///////////////////////////////////////////////////////////////
/////////////////// edit single recipe ////////////////////////
///////////////////////////////////////////////////////////////

export function editRecipe(recipeId, formData, signedIn, authToken, tokenType, client, expiry, uid) {
	if(signedIn === false || signedIn === null || signedIn === undefined) {
		return { type: actions.EDIT_RECIPE_REQUEST };
	}

	return function(dispatch) {
		let theResponse;
		dispatch({type: actions.EDIT_RECIPE_REQUEST});

		fetch(`${settings.apiBaseUrl}/api/recipes/${recipeId}`, {
			method: 'PUT',
			headers: {
				'Accept': settings.apiVersionHeader,
				'Access-Token': authToken,
				'Client': client,
				'Token-Type': tokenType,
				'Expiry': expiry,
				'uid': uid
			},
			body: formData
		})
		.then(response => {
			theResponse = response;
			return response.json();
		})
		.then(json => {
			checkStatus(theResponse, dispatch, signedIn);
			if(_.isUndefined(json.recipe)) {
				let error = new Error(theResponse.statusText);
				error.response = json.errors;
				throw error;
			}
			else {
				dispatch(setAlert("Recipe updated successfully", AlertTypes.SUCCESS));
				dispatch(successEditRecipe(json));
				dispatch(updateRecipeInList(json.recipe));
				dispatch(updatedSelectedRecipe(json.recipe));
				browserHistory.push(`/recipes/${recipeId}`);
			}
		})
		.catch(error => {
			dispatch(rejectEditRecipe(error));
		});
		return null;
	};
}

export function successEditRecipe(json) {
  return {
    type: actions.EDIT_RECIPE_SUCCESS,
    recipe: json
  };
}

export function rejectEditRecipe(error) {
  return {
    type: actions.EDIT_RECIPE_FAILURE,
    error: error.toString()
  };
}

///////////////////////////////////////////////////////////////
///////// get available step class list from server ///////////
///////////////////////////////////////////////////////////////

export function getAvailableStepClasses(appState) {
	const { authToken, tokenType, client, expiry, uid } = appState.session;
	const signedIn = (authToken != null);

	if(signedIn === false || _.isNil(signedIn)) {
		return { type: actions.AVAILABLE_STEP_LIST_REQUEST };
	}

	return function(dispatch) {
		let theResponse;
		dispatch({type: actions.AVAILABLE_STEP_LIST_REQUEST});

		fetch(`${settings.apiBaseUrl}/api/available_step_classes`, {
			method: 'GET',
			headers: {
				'Accept': settings.apiVersionHeader,
				'Access-Token': authToken,
				'Client': client,
				'Token-Type': tokenType,
				'Expiry': expiry,
				'uid': uid
			}
		})
		.then(response => {
			theResponse = response;
			return response.json();
		})
		.then(json => {
			checkStatus(theResponse, dispatch, signedIn);
			if(_.isUndefined(json.available_step_classes)) {
				let error = new Error(theResponse.statusText);
				error.response = json.errors;
				throw error;
			}
			else {
				dispatch(successGetAvailableStepClasses(json));
			}
		})
		.catch(error => {
			dispatch(rejectGetAvailableStepClasses(error));
		});
		return null;
	};
}

export function successGetAvailableStepClasses(json) {
  return {
    type: actions.AVAILABLE_STEP_LIST_SUCCESS,
    available_step_classes: json.available_step_classes
  };
}

export function rejectGetAvailableStepClasses(error) {
  return {
    type: actions.AVAILABLE_STEP_LIST_FAILURE,
    error: error.toString()
  };
}
