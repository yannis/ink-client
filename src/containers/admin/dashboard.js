import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { browserHistory, Link } from 'react-router';
import _ from 'lodash';

import * as actions from '../../actions/adminActions.js';
import { getAvailableStepClasses } from '../../actions/recipeActions';

import Header from '../../components/header/Header';

import AccountsList from '../../components/admin/AccountsList';
import StatusReport from '../../components/admin/StatusReport';
import AlertList from '../../components/AlertList';
import { setAlert } from '../../actions/actions_helper';

class AdminDashboardPage extends Component {
  static propTypes = {
    actions: PropTypes.object.isRequired,
    appState: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
  };

  componentWillMount = () => {
    this.checkAuth();
  }

  componentDidMount = () => {
    const { appState, dispatch } = this.props;
    this.checkAuth();
    dispatch(actions.getAllAccounts(appState));
    dispatch(actions.getStatusReport(appState));
    dispatch(getAvailableStepClasses(appState));
  }

  checkAuth() {
    const { appState, dispatch } = this.props;
    const { session } = appState;

    if(_.isNull(session.account)) {
      dispatch(setAlert("Please sign in"));
      browserHistory.push('/signin');
      return;
    } else if(session.account.admin != true) {
      dispatch(setAlert("Authorised accounts only"));
      browserHistory.push('/');
      return;
    }
  }

  renderStatusReport(appState) {
    return(
      <StatusReport
        statusReport={appState.admin.statusReport}
        getStatusReportInProgress={appState.admin.getStatusReportInProgress}
        session={appState.session}
      />
    );
  }

  renderStepClassItem(klass) {
    return(
      <div className="admin-step-class-item" key={klass.name}>
        <div className="">{klass.name}</div>
        <div className="step step--blue step__admin-description">{klass.description}</div>
      </div>
    );
  }

  renderAvailableStepClasses(appState) {
    if(_.isEmpty(appState.availableStepClassList) || _.isNil(appState.availableStepClassList)) {
      return(
        <div className="loading-centered">
          <div><span className="fa fa-cog fa-spin fa-3x fa-fw" /></div>
          <div><span className="small-info">loading...</span></div>
        </div>
      );
    }
    return(
      appState.availableStepClassList.map(klass => this.renderStepClassItem(klass))
    );
  }

  renderAccountList(appState) {
    if(_.isEmpty(appState.admin.accounts) || appState.admin.getAccountsInProgress == true) {
      return(
        <div className="loading-centered">
          <div><span className="fa fa-cog fa-spin fa-3x fa-fw" /></div>
          <div><span className="small-info">loading...</span></div>
        </div>
      );
    }
    else {
      return(
        <div>
          <AccountsList
            accounts={appState.admin.accounts}
            getAccountsInProgress={appState.admin.getAccountsInProgress}
            session={appState.session}
          />
        </div>
      );
    }
  }

  pageContent(appState) {
    return(
      <div className="recipe-detail-view-container">
        <div>
          <div>
            <h4>status report</h4>
            { this.renderStatusReport(appState) }
          </div>
          <div>
            <h4>available step classes</h4>
            { this.renderAvailableStepClasses(appState) }
          </div>
        </div>
        <div>
          <h4>accounts</h4>
          { this.renderAccountList(appState) }
        </div>
      </div>
    );
  }

  render() {
    const { appState } = this.props;

    return (
      <div>
        <Header
          appState={appState}
        />
        <div className="content-container">
          <div className="breadcrumb-container">
            <Link to="/" className="breadcrumb">Home</Link>
            <span className="breadcrumb-divider">&gt;&gt;</span>
            <Link to="/admin/accounts" className="breadcrumb"><span className="fa fa-accounts-o"/> Accounts</Link>
          </div>

          <AlertList
            alerts={appState.alerts} />
          <h1>Admin dashboard</h1>
          {this.pageContent(appState)}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    appState: state.appState,
    alerts: PropTypes.array.isRequired,
    recipes: PropTypes.array.isRequired
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    dispatch: dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdminDashboardPage);
