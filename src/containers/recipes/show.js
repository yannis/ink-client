import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { browserHistory } from 'react-router';
import { Link } from 'react-router';

import * as actions from '../../actions/recipeActions';
import _ from 'lodash';
import { getAllRecipes, resetPlaceholders } from '../../actions/recipeActions.js';
import { setAlert } from '../../actions/actions_helper';

import Header from '../../components/header/Header';
import AlertList from '../../components/AlertList';
import ExecutionFileForm from '../../components/ExecutionFileForm';
import RecipeStepDetail from '../../components/RecipeStepDetail';
import ExecutionList from '../../components/ExecutionList';

class RecipeShowPage extends Component {
  static propTypes = {
    actions: PropTypes.object.isRequired,
    appState: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    params: PropTypes.object,
    recipe: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount = () => {
    let { dispatch, appState, recipe, params } = this.props;
    const { session } = appState;
    dispatch(resetPlaceholders());

    if(_.isNull(session.account)) {
      dispatch(setAlert("Please sign in"));
      browserHistory.push('/signin');
      return;
    }

    if(_.isNull(appState.recipes) || _.isNull(recipe)) {
      dispatch(getAllRecipes(appState));
    }
  }

  findRecipe() {
    let { appState, params, dispatch } = this.props;
    let recipeId = _.toNumber(this.props.params.id);
    if(appState.selectedRecipe && appState.selectedRecipe.id === recipeId)
      return(appState.selectedRecipe);

    let recipe = _.find(appState.recipes, _.matchesProperty('id', recipeId));
    if(!_.isNull(recipe) && !_.isUndefined(recipe)) {
      return(recipe);
    }
  }

  renderEditLink(recipe) {
    const { session } = this.props.appState;
    if(recipe.account_id == session.account.id) {
      return(
        <Link to={`/recipes/${recipe.id}/edit`}><span className="fa fa-pencil"/> edit this recipe</Link>
      );
    }
  }

  currentRecipeLink(recipe) {
    return(`/recipes/${recipe.id}`);
  }

  renderExecutionList(recipe) {
    let { appState } = this.props;
    if(_.isEmpty(recipe.process_chains) && appState.executionPlaceholderCount == 0) {
      return(<p>This recipe has not been executed by you yet.</p>);
    }
    let { session } = this.props.appState;
    if(recipe){
      return(
        <ExecutionList
          recipe={recipe}
          session={session}
          executionPlaceholderCount = {appState.executionPlaceholderCount}
        />
      );
    } else {
      return(<span/>);
    }
  }

  renderRecipeSteps(recipeSteps) {
    if(_.isEmpty(recipeSteps)) {
      return(<p>No steps! This recipe is not very useful.</p>);
    }
    else {
      return(
        recipeSteps.map(recipeStep =>
          <RecipeStepDetail
            key={recipeStep.id}
            recipeStep={recipeStep}
          />)
      );
    }
  }

  content(recipe, appState) {
    if(_.isEmpty(appState.recipes)) {
      return(
        <div className="loading-centered">
          <div><span className="fa fa-gear fa-spin fa-3x fa-fw" /></div>
          <div><span className="small-info">Loading...</span></div>
        </div>
      );
    }

    if (_.isEmpty(recipe)) {
      return(
        <div className="loading-centered">
          <div><span className="fa fa-question fa-spin fa-3x fa-fw" /></div>
          <div><span className="small-info">I think that recipe might not exist...</span></div>
          <div><Link to="/recipes">back to recipe list</Link></div>
        </div>
      );
    }

    return (
      <div className="content-container">
        <div className="breadcrumb-container">
          <Link to="/" className="breadcrumb">Home</Link>
          <span className="breadcrumb-divider">&gt;&gt;</span>
          <Link to="/recipes" className="breadcrumb"><span className="fa fa-calendar-o"/> Recipes</Link>
          <span className="breadcrumb-divider">&gt;&gt;</span>
          <Link to={this.currentRecipeLink(recipe)} className="breadcrumb">{recipe.name}</Link>
        </div>

        <AlertList
          alerts={appState.alerts} />

        <div className="recipe-detail-view">
          <h1>Recipe: {recipe.name}</h1>
          <h4>{recipe.description}</h4>
          {this.renderEditLink(recipe)}

          <ExecutionFileForm recipe={recipe} />
          <div className="recipe-detail-view-container">
            <div>
              <h3>Steps</h3>
              <div className="steps-container">
                {this.renderRecipeSteps(recipe.recipe_steps)}
              </div>
            </div>
            <div className="recipe-execution-history-container">
              <h3>Previous processes ({recipe.process_chains.length + appState.executionPlaceholderCount})</h3>
              {this.renderExecutionList(recipe)}
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const { dispatch, appState } = this.props;
    let recipe = this.findRecipe();

    return (
      <div>
        <Header
          appState={appState}
        />
        {this.content(recipe, appState)}
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    appState: state.appState
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    dispatch: dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecipeShowPage);
