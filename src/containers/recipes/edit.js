import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { browserHistory } from 'react-router';
import { Link } from 'react-router';

import * as actions from '../../actions/recipeActions';
import _ from 'lodash';

import Header from '../../components/header/Header';
import EditRecipeForm from '../../components/EditRecipeForm';
import AlertList from '../../components/AlertList';
import { getAllRecipes } from '../../actions/recipeActions.js';
import { setAlert } from '../../actions/actions_helper';

class RecipeEditPage extends Component {
  static propTypes = {
    actions: PropTypes.object.isRequired,
    appState: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    params: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount = () => {
    let { dispatch, appState, params } = this.props;
    const { session } = appState;

    if(_.isNull(session.account)) {
      dispatch(setAlert("Please sign in"));
      browserHistory.push('/signin');
      return;
    }

    const recipe = this.findRecipe();
  }

  componentDidMount = () => {
    let { dispatch, appState, params } = this.props;
    const { session } = appState;

    if(_.isNull(appState.recipes)) {
      dispatch(getAllRecipes(appState));
    }

    dispatch(actions.selectRecipe(params.id, appState));
    dispatch(actions.getRecipe(params.id, appState));
  }

  findRecipe() {
    let { appState, params, dispatch } = this.props;
    let recipeId = _.toNumber(this.props.params.id);
    if(appState.selectedRecipe && appState.selectedRecipe.id === recipeId)
      return(recipe);

    let recipe = _.find(appState.recipes, _.matchesProperty('id', recipeId));
    if(!_.isNull(recipe) && !_.isUndefined(recipe)) {
      return(recipe);
    }
  }

  currentRecipeLink(recipe) {
    return(`/recipes/${recipe.id}/edit`);
  }

  content(recipe, appState) {
    if (_.isEmpty(appState.recipes)) {
      return(
        <div className="loading-centered">
          <div><span className="fa fa-cog fa-spin fa-3x fa-fw" /></div>
          <div><span className="small-info">recipes loading...</span></div>
        </div>
      );
    }
    if (_.isEmpty(recipe) && !_.isEmpty(appState.recipes)) {
      return(
        <div className="loading-centered">
          <div><span className="fa fa-question fa-spin fa-3x fa-fw" /></div>
          <div><span className="small-info">I think that recipe might not exist...</span></div>
          <div><Link to="/recipes">back to recipe list</Link></div>
        </div>
      );
    }

    return (
      <div className="content-container">
        <div className="breadcrumb-container">
          <Link to="/" className="breadcrumb">Home</Link>
          <span className="breadcrumb-divider">&gt;&gt;</span>
          <Link to="/recipes" className="breadcrumb"><span className="fa fa-calendar-o"/> Recipes</Link>
          <span className="breadcrumb-divider">&gt;&gt;</span>
          <Link to={this.currentRecipeLink(recipe)} className="breadcrumb">{recipe.name}</Link>
        </div>

        <AlertList
          alerts={appState.alerts} />

        <div className="recipe-detail-view">
          <h1>Edit Recipe - {recipe.name}</h1>
          <h4>{recipe.description}</h4>

          <EditRecipeForm recipe={recipe} />
        </div>
      </div>
    );
  }

  render() {
    const { dispatch, appState } = this.props;
    const { session } = this.props.appState;
    const recipe = appState.selectedRecipe || this.findRecipe(appState.recipes);

    return (
      <div>
        <Header
          appState={appState}
        />
        {this.content(recipe, appState)}
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    appState: state.appState
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    dispatch: dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecipeEditPage);
