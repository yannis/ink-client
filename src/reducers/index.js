import { combineReducers } from 'redux';
import appState from './reducer';

const rootReducer = combineReducers({
  appState
});

export default rootReducer;
